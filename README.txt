This README is for the code in the bachelorproject interaction in multi-agent system 
written by Thomas Kj�rgaard Malowanczyk at the Technical university of Denmark.

The code for this project consist of a MAS program in the "GOAL" folder and an environment for the MAS in the "Environment" folder.

-------- GOAL -------
This folder consist of 6 files used in the agent-programming language GOAL
'GoalSimpleJim.mas2g' is the main file for the multi-agent system.
The 'Jim.goal' file is the agent and uses the 3 files 'emotion.mod2g', 'emotionHandler.mod2g' and 'planner.mod2g'.
The last file is the "SimpleJim.jar" file which is the environment used in the multi-agent system. 
This jar file is constructed from the java files in the "Environment" folder

-------- Environment -------
This folder contains the files for the Environment used in the multi-agent system.
The 5 java files are needed to build the jar file, this jar file is build with antlr (ANother Tool for Language Recognition) and uses the 'build.xml' file.
All of  the java files are needed to build the environment.

