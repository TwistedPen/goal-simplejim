package SimpleJim;

import SimpleJim.EnvironmentInterface.AgentAction;
import SimpleJim.EnvironmentInterface.UserAction;
import eis.exceptions.ManagementException;

public class Environment {
	
	private EnvironmentInterface environmentInterface;
	public boolean debug = false;
	private EnvironmentWindow window;
	public Agent agent;
	UserAction lastUserAction = UserAction.NOTHING;
	boolean userPerformedAction = false;
	boolean agentPerformedAction = false;
	boolean agentWitnessedAction = false;
	AgentAction pendingUserAction;
	AgentAction pendingAgentAction;
	private long time = 0;

	public Environment(EnvironmentInterface environmentInterface, boolean money, boolean food, int energy, int hunger, boolean debug){
		
		this.environmentInterface = environmentInterface;
		this.debug = debug;
		
		String entity = "agent";
		agent = new Agent(this, money, food, energy, hunger);
		
		environmentInterface.addAgentEntity(entity);
		window = new EnvironmentWindow(this);
		
		resetState();
	}
	
	public EnvironmentWindow getEnvWin(){
		return window;
	}

	private void resetState() {
		window.repaint();
	}
	
	public long getTime() {
		return time;
	}
	
	public void performUserAction(UserAction action) {

		// resets the text are for emotions
		getEnvWin().emotionText.setText("");

		//records the last user action
		lastUserAction = action;
		
		// Checks if the agent witnessed an user action or not
		switch(agent.currentState){
		case SHOPPING:
		case SLEEPING:
		case STEALINGFOOD:
		case STEALINGMONEY:
		case WORKING:
			switch(action){
			case GIVEFOOD:
			case GIVEMONEY:
				agentWitnessedAction = false;
				break;
			case TAKEFOOD:
				if(agent.food)
					agentWitnessedAction = false;
				break;
			case TAKEMONEY:
				if(agent.money)
					agentWitnessedAction = false;
			default:
				break;
			}
			break;
		default:
			switch(action){
			case GIVEFOOD:
			case GIVEMONEY:
				agentWitnessedAction = true;
				break;
			case TAKEFOOD:
				if(agent.food)
					agentWitnessedAction = true;
				break;
			case TAKEMONEY:
				if(agent.money)
					agentWitnessedAction = true;
			default:
				break;
			}
			break;
		}	
		
		boolean noFood = false;
		
		// apply changes to the environment
		switch(action){
		case GIVEFOOD:
			agent.food = true;
			break;
		case GIVEMONEY:
			agent.money = true;
			break;
		case TAKEFOOD:
			if( agent.food && agent.currentState != Agent.State.EATING)
				agent.food = false;
			break;
		case TAKEMONEY:
			if(agent.money && agent.currentState != Agent.State.SHOPPING){
				agent.money = false;
			} else {
				noFood = true;
			}
			break;
		default:
			break;	
		}
		
		// perform agents action
		if(agentPerformedAction) {
			agentUpdateAction(pendingAgentAction);
		}
		
		if (noFood){
			agent.food = false;
		}
	}
	
	// manage the agents action
	public void performAction(String entity, AgentAction action) {

		pendingAgentAction = action;
		agentPerformedAction = true;
		
		if(agentPerformedAction && !userPerformedAction){
			return;
		} else {
			agentUpdateAction(action);
		}

	}
	
	public void agentUpdateAction(AgentAction action){

		// apply agent action cost to the agent
		if(action.equals(AgentAction.RETURN)){
			switch(agent.currentState){
			case EATING:
				if(agent.food){
					agent.food = false;
					agent.hunger = 0;
					agent.energy -= agent.energy == 0 ?  0 :  1;
				}
				break;
			case SHOPPING:
				if(agent.money){
					agent.food = true;
					agent.money = false;
					agent.hunger += agent.hunger == 0 ?  0 :  1;
					agent.energy -= agent.energy == 0 ?  0 :  1;
				}
				break;
			case SLEEPING:
				agent.energy = 5;
				agent.hunger += agent.hunger >= 4 ?  (agent.hunger == 4 ? 1 : 0 ) :  2;
				break;
			case STEALINGFOOD:
				agent.food = true;
				break;
			case STEALINGMONEY:
				agent.money = true;
				break;
			case WORKING:
				agent.money = true;
				agent.hunger += agent.hunger >= 4 ?  (agent.hunger == 4 ? 1 : 0 ) :  2;
				agent.energy -= agent.energy <= 1 ?  (agent.energy == 1 ? 1 : 0 ) :  2;
				break;
			case UNKNOWN:
				break;
			default:
				break;

			}
		}
		
		// update the state of the agent
		agent.processAction(action);
		userPerformedAction = false;
		agentPerformedAction = false;
		
		// increment time
		time++;
		
		window.repaint();
	}

	public void terminate() {
		
		environmentInterface.deleteAgentEntity("agent");

		window.dispose();
		window.setVisible(false);
		window = null;
		if (environmentInterface != null) {
			try {
				environmentInterface.kill();
			} catch (ManagementException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
}
