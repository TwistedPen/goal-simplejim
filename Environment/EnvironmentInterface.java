package SimpleJim;

import java.util.LinkedList;
import java.util.Map;
import eis.EIDefaultImpl;
import eis.exceptions.*;
import eis.iilang.*;

public class EnvironmentInterface extends EIDefaultImpl {
	
	private static final long serialVersionUID = 1L;
	private Environment environment;
	LinkedList<Parameter> p = null;

	// enumerator for initial variables received from GOAL
	enum InitKey {
		MONEY, FOOD, ENERGY, DEBUG, UNKNOWN, HUNGER;

		static InitKey toEnum(String string) {
			try {
				return valueOf(string.replaceAll("([A-Z])", "_$1").toUpperCase());
			} catch (Exception e) {
				return UNKNOWN;
			}
		}

		static String toString(InitKey key) {
			String[] strings = key.toString().split("_");
			StringBuilder sb = new StringBuilder(strings[0].toLowerCase());
			for (int i = 1; i < strings.length; i++) {
				sb.append(strings[i].substring(0, 1));
				sb.append(strings[i].substring(1).toLowerCase());
			}
			return sb.toString();
		}
	}

	enum AgentAction {
		WORK, STEALMONEY, EAT, BUYFOOD, STEALFOOD, SLEEP, RETURN, WAIT, EMOTIONVERBAL, EMOTIONDISPLAY, UNKNOWN;

		static AgentAction toEnum(String string) {
			try {
				return valueOf(string.replaceAll("([A-Z])", "_$1").toUpperCase());
			} catch (Exception e) {
				return UNKNOWN;
			}
		}

		static String toString(AgentAction action) {
			String[] strings = action.toString().split("_");
			StringBuilder sb = new StringBuilder(strings[0].toLowerCase());
			for (int i = 1; i < strings.length; i++) {
				sb.append(strings[i].substring(0, 1));
				sb.append(strings[i].substring(1).toLowerCase());
			}
			return sb.toString();
		}
	}
	
	enum UserAction {
		GIVEMONEY, TAKEMONEY, GIVEFOOD, TAKEFOOD, NOTHING, UNKNOWN;

		static UserAction toEnum(String string) {
			try {
				return valueOf(string.replaceAll("([A-Z])", "_$1").toUpperCase());
			} catch (Exception e) {
				return UNKNOWN;
			}
		}

		static String toString(AgentAction action) {
			String[] strings = action.toString().split("_");
			StringBuilder sb = new StringBuilder(strings[0].toLowerCase());
			for (int i = 1; i < strings.length; i++) {
				sb.append(strings[i].substring(0, 1));
				sb.append(strings[i].substring(1).toLowerCase());
			}
			return sb.toString();
		}
	}

	public EnvironmentInterface() {
		super();
	}

	public static void main(String[] args) {
	}

	public void addAgentEntity(String entity) {
		try {
			addEntity(entity, "agent");
		} catch (EntityException e) {
			e.printStackTrace();
		}
	}

	public void deleteAgentEntity(String entity) {

		try {
			deleteEntity(entity);
		} catch (RelationException e) {
			e.printStackTrace();
		} catch (EntityException e) {
			e.printStackTrace();
		}
	}

	// Apply the initial variables received from GOAL to the environment 
	@Override
	public void init(Map<String, Parameter> parameters) throws ManagementException {
		super.init(parameters);

		// defaults
		boolean food = false;
		boolean money = false;
		int energy = 5;
		int hunger = 0;
		boolean debug = false;

		for (String key : parameters.keySet()) {
			Parameter parameter = parameters.get(key);
			switch (InitKey.toEnum(key)) {

				case MONEY:
					money = true;				
				case FOOD:
					food = true;
				case ENERGY:
					if (!(parameter instanceof Numeral))
						throw new ManagementException("Expected numeral");
					energy = ((Numeral) parameter).getValue().intValue();
					if (energy < 0)
						throw new ManagementException("Expected positive value");
					break;
				case HUNGER:
					if (!(parameter instanceof Numeral))
						throw new ManagementException("Expected numeral");
					hunger = ((Numeral) parameter).getValue().intValue();
					if (hunger < 0)
						throw new ManagementException("Expected positive value");
					break;
				case DEBUG:
					debug = true;
					break;
				default:
					throw new ManagementException("Unknown initialization key: " + key);
			}
		}

		environment = new Environment(this, money, food, energy, hunger, debug);

		pause();
		start();
	}

	@Override
	public String requiredVersion() {
		return "0.3";
	}

	// manage percepts to agents
	@Override
	protected LinkedList<Percept> getAllPerceptsFromEntity(String entity) throws PerceiveException, NoEnvironmentException {
		LinkedList<Percept> percepts = new LinkedList<Percept>();
		
		Agent a = environment.agent;
		
		//global percepts
		
		// current state
		percepts.add(new Percept("state",
				new Identifier(a.currentState.toString())
		));
		//food and money
		if(a.food){
			percepts.add(new Percept(
					"food"
			));
		} else if (a.money)
		percepts.add(new Percept(
				"money"
		));
		
		// energy
		percepts.add(new Percept(
				"energy",
				new Numeral(a.energy)
		));
		
		//hunger
		percepts.add(new Percept(
				"hunger",
				new Numeral(a.hunger)
		));
		
		percepts.add(new Percept(
				"time",
				new Numeral(environment.getTime())
		));
		
		// percepts for obtained or stolen items along with percepts if the agent witnessed it. 
		switch(environment.lastUserAction){
		case GIVEFOOD:
			percepts.add(new Percept(
					"given",
					new Identifier("food")
			));
			if(environment.agentWitnessedAction){
				percepts.add(new Percept(
						"userPerformed",
						new Identifier(environment.lastUserAction.toString())
				));
			}
			break;
		case GIVEMONEY:
			percepts.add(new Percept(
					"given",
					new Identifier("money")
			));
			if(environment.agentWitnessedAction){
				percepts.add(new Percept(
						"userPerformed",
						new Identifier(environment.lastUserAction.toString())
				));
			}
			break;
		case TAKEFOOD:
			percepts.add(new Percept(
					"lost",
					new Identifier("food")
			));
			if(environment.agentWitnessedAction){
				percepts.add(new Percept(
						"userPerformed",
						new Identifier(environment.lastUserAction.toString())
				));
			}
			break;
		case TAKEMONEY:
			percepts.add(new Percept(
					"lost",
					new Identifier("money")
			));
			if(environment.agentWitnessedAction){
				percepts.add(new Percept(
						"userPerformed",
						new Identifier(environment.lastUserAction.toString())
				));
			}
			break;
		default:
			break;
		
		}
		
		// resets the users last action
		if(environment.lastUserAction != UserAction.NOTHING) 
			environment.lastUserAction = UserAction.NOTHING;

		return percepts;
}

	@Override
	protected boolean isSupportedByEnvironment(Action action) {
		return AgentAction.toEnum(action.getName()) != AgentAction.UNKNOWN;
	}

	@Override
	protected boolean isSupportedByType(Action action, String type) {
		return AgentAction.toEnum(action.getName()) != AgentAction.UNKNOWN && type.equals("actor");
	}

	@Override
	protected boolean isSupportedByEntity(Action action, String entity) {
		return AgentAction.toEnum(action.getName()) != AgentAction.UNKNOWN && getEntities().contains(entity);
	}

	// manage action received from GOAL
	@Override
	protected Percept performEntityAction(String entity, Action action) throws ActException {
		
		AgentAction actorAction = AgentAction.toEnum(action.getName());
		
		if(actorAction == AgentAction.EMOTIONDISPLAY) {
			String emotion = ((Identifier)action.getParameters().get(0)).getValue();
			environment.agent.displayEmotion(entity, actorAction,  emotion);
		}else if(actorAction == AgentAction.EMOTIONVERBAL) {
			
			if (action.getParameters().size() == 5){
				
				String predicate = ((Identifier)action.getParameters().get(0)).getValue();
				String agent1 = ((Identifier)action.getParameters().get(1)).getValue();
				String agent2 = ((Identifier)action.getParameters().get(2)).getValue();
				String object = ((Identifier)action.getParameters().get(3)).getValue();
				int intensity = ((Numeral) action.getParameters().get(4)).getValue().intValue();
				
				environment.agent.sayEmotion(entity, actorAction, predicate, agent1, agent2, object, intensity );
				
			} else if (action.getParameters().size() == 4) {
				
				String predicate = ((Identifier)action.getParameters().get(0)).getValue();
				String agent = ((Identifier)action.getParameters().get(1)).getValue();
				String object = ((Identifier)action.getParameters().get(2)).getValue();
				int intensity = ((Numeral) action.getParameters().get(3)).getValue().intValue();
				
				environment.agent.sayEmotion(entity, actorAction, predicate, agent, object, intensity );
				
			} else{
				throw new ActException(ActException.NOTSUPPORTEDBYENVIRONMENT);
			}
			
		}else if(actorAction != AgentAction.UNKNOWN) {
			environment.performAction(entity, actorAction);
		}else {
			throw new ActException(ActException.NOTSUPPORTEDBYENVIRONMENT);
		}
		
		return null;
	}

	public Environment getEnvironment() {
		return environment;
	}
}
