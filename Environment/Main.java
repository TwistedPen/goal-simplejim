package SimpleJim;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import SimpleJim.EnvironmentInterface.AgentAction;
import eis.exceptions.ActException;
import eis.exceptions.AgentException;
import eis.exceptions.ManagementException;
import eis.exceptions.NoEnvironmentException;
import eis.exceptions.PerceiveException;
import eis.exceptions.RelationException;
import eis.iilang.Action;
import eis.iilang.Numeral;
import eis.iilang.Parameter;
import eis.iilang.Percept;

abstract class AgentTest extends Thread {
	protected EnvironmentInterface ei = null;
	protected String id = null;

	public AgentTest(EnvironmentInterface ei, String id) {
		this.ei = ei;
		this.id = id;
	}

	protected void say(String msg) {
		System.out.println(id + " says: " + msg);
	}
}

class RandomAgent extends AgentTest {
	private static Random random = new Random();

	public RandomAgent(EnvironmentInterface ei, String id) {
		super(ei, id);
		
	}
	

	@Override
	public void run() {
		try {
			ei.associateEntity(id, ei.getFreeEntities().getFirst());
			Agent agent = ei.getEnvironment().agent;

			while (true) {
				// perceive
				Map<String, Collection<Percept>> percepts = ei.getAllPercepts(id);
				long time = ei.getEnvironment().getTime();

				Action randomAction;
				
				if(agent.currentState.equals(Agent.State.HOME)){
					randomAction = new Action(AgentAction.toString(AgentAction.values()[7]));
				} else if(agent.currentState.equals(Agent.State.WAITING)){
					randomAction = new Action(AgentAction.toString(AgentAction.values()[random.nextInt(5)]));
				} else{ 
					randomAction = new Action(AgentAction.toString(AgentAction.values()[6]));
				}
				
				System.out.println("Random action is: " + randomAction.toString());
				ei.performAction(id, randomAction);

				// block until next step
				while (time == ei.getEnvironment().getTime()) {
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (PerceiveException e) {
			e.printStackTrace();
		} catch (RelationException e) {
			e.printStackTrace();
		} catch (ActException e) {
			e.printStackTrace();
		} catch (NoEnvironmentException e) {
			e.printStackTrace();
		}
	}
}

public class Main {
	public static void main(String[] args) {
		
		try {
			EnvironmentInterface environmentInterface = new EnvironmentInterface();
			
			try {
				Map<String, Parameter> parameters = new HashMap<String, Parameter>();
				parameters.put("energy", new Numeral(5));
				parameters.put("hunger", new Numeral(0));
				environmentInterface.init(parameters);
			} catch (ManagementException e) {
				e.printStackTrace();
			}
			
			Thread.sleep(250);
			AgentTest a1 = new RandomAgent(environmentInterface, "a1");		
	
			environmentInterface.registerAgent("a1");
	
			a1.start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (AgentException e) {
			e.printStackTrace();
		}

	}
}

