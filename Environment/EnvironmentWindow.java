package SimpleJim;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import javax.swing.*;

import SimpleJim.EnvironmentInterface.UserAction;

public class EnvironmentWindow extends JFrame implements WindowListener {
	private static final long serialVersionUID = 1L;
	private Environment env;
	private ViewPanel viewPanel;
	private JPanel mainFrame, control, agentVar, debugPanel, emotion;
	private JLabel energy, money, food, agentState, hunger;
	public JLabel expression;
	public JTextArea emotionText;
	public JLabel debugLabel = new JLabel("debug variable: ");;
	
	//Setup the environment with the initial settings
	public EnvironmentWindow(Environment environment) {
	
		this.env = environment;
		
		setTitle("SimpleJim Environment");
		setSize(500, 500);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(this);

		viewPanel = new ViewPanel();
		mainFrame = new JPanel();
		control = new JPanel();
		agentVar = new JPanel();
		debugPanel = new JPanel();
		emotion = new JPanel();
		energy = new JLabel("Energy: " + env.agent.energy , JLabel.LEADING);
		hunger = new JLabel("Hunger: " + env.agent.hunger , JLabel.LEADING);
		money = new JLabel("Has money: " + env.agent.money , JLabel.LEADING);
		food = new JLabel("Has food: " + env.agent.food , JLabel.LEADING);
		expression = new JLabel(env.agent.disEmotion.toString(), JLabel.LEADING);
		agentState = new JLabel("State: " + env.agent.currentState , JLabel.LEADING);
		
		emotionText = new JTextArea();
		emotionText.setEditable(false);
		JScrollPane sp = new JScrollPane(emotionText,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		sp.setPreferredSize(new Dimension(400, 150));
		sp.setBounds(0, 0, 600, 500);
		emotion.add(sp);

		add(mainFrame);
		viewPanel.setPreferredSize(new Dimension(200,200));
		mainFrame.add(viewPanel);
		mainFrame.add(agentVar);
		mainFrame.add(control);
		mainFrame.add(emotion);
		
		if(env.debug){
			mainFrame.add(debugPanel);
			debugLabel = new JLabel("debug variable: ");
			debugPanel.add(debugLabel);
		}
		showButton();
		agentVar.add(energy);
		agentVar.add(hunger);
		agentVar.add(money);
		agentVar.add(food);
		agentVar.add(agentState);
		viewPanel.add(expression);
		
		setVisible(true);
		
	}

	class ViewPanel extends JPanel {
		
		@Override
		public void paintComponent(Graphics g_) {
			super.paintComponents(g_);

			Graphics2D g = (Graphics2D) g_;
			Graphics2D gg;
						
			env.agent.draw(g, this);
			updateAgentVar();
	
		}		
	}
	
	public void updateAgentVar(){
		
		//Updates the text of the agents variables
		energy.setText("Energy: " + env.agent.energy);
		hunger.setText("Hunger: " + env.agent.hunger);
		money.setText("Has money: " + env.agent.money);
		food.setText("Has food: " + env.agent.food);
		agentState.setText("State: " + env.agent.currentState);
		expression.setText(env.agent.disEmotion.toString());
	}
	
	public void showButton(){
		
		// add buttons to the window
		JButton tmb = new JButton("Take Money");
		tmb.setActionCommand("TAKEMONEY");
		
		JButton doNothing = new JButton("Do Nothing");
		doNothing.setActionCommand("NOTHING");
		
		JButton gmb = new JButton("Give Money");
		gmb.setActionCommand("GIVEMONEY");
		
		JButton tfb = new JButton("Take food");
		tfb.setActionCommand("TAKEFOOD");
		
		JButton gfb = new JButton("Give food");
		gfb.setActionCommand("GIVEFOOD");

		doNothing.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				env.performUserAction(UserAction.NOTHING);
			}          
		});
		
		tmb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				env.performUserAction(UserAction.TAKEMONEY);
				updateAgentVar();
			}          
		});
		gmb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				env.performUserAction(UserAction.GIVEMONEY);
				updateAgentVar();
			}          
		});
		tfb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				env.performUserAction(UserAction.TAKEFOOD);
				updateAgentVar();
			}          
		});
		gfb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				env.performUserAction(UserAction.GIVEFOOD);
				updateAgentVar();
			}          
		});
		
		control.add(tmb);
		control.add(gmb);
		control.add(tfb);
		control.add(gfb);
		control.add(doNothing);
		this.setVisible(true);
		
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		env.terminate();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

}
