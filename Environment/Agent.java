package SimpleJim;

import java.awt.Color;
import java.awt.Graphics2D;
import javax.swing.JTextArea;
import SimpleJim.EnvironmentInterface.AgentAction;
import SimpleJim.EnvironmentWindow.ViewPanel;

public class Agent {
	
	public Environment env;
	boolean food = false;
	boolean money = false;
	int energy = 5;
	int hunger = 0;
	State currentState = State.WAITING;
	displayingEmotion disEmotion = displayingEmotion.NOTHING;
	
	public Agent(Environment environment, boolean money2, boolean food2, int energy2, int hunger2){
		this.env = environment;
		this.money = money2;
		this.energy = energy2;
		this.food = food2;
		this.hunger = hunger2;
	}
	
	enum displayingEmotion {
		HAPPY, SAD, FEAR, ANGRY, DISGUST, NOTHING, UNKNOWN;
		
		static displayingEmotion toEnum(String string) {
			try {
				return valueOf(string.replaceAll("([A-Z])", "_$1").toUpperCase());
			} catch (Exception e) {
				return UNKNOWN;
			}
		}

		static String toString(AgentAction action) {
			String[] strings = action.toString().split("_");
			StringBuilder sb = new StringBuilder(strings[0].toLowerCase());
			for (int i = 1; i < strings.length; i++) {
				sb.append(strings[i].substring(0, 1));
				sb.append(strings[i].substring(1).toLowerCase());
			}
			return sb.toString();
		}
	}
	
	enum State {
		HOME, EATING, SLEEPING, WORKING, SHOPPING, STEALINGFOOD, STEALINGMONEY, WAITING, UNKNOWN;
		
		static State toEnum(String string) {
			try {
				return valueOf(string.replaceAll("([A-Z])", "_$1").toUpperCase());
			} catch (Exception e) {
				return UNKNOWN;
			}
		}

		static String toString(AgentAction action) {
			String[] strings = action.toString().split("_");
			StringBuilder sb = new StringBuilder(strings[0].toLowerCase());
			for (int i = 1; i < strings.length; i++) {
				sb.append(strings[i].substring(0, 1));
				sb.append(strings[i].substring(1).toLowerCase());
			}
			return sb.toString();
		}
	}
	
	public void processAction(AgentAction action) {
		
		// change the agent state
		if(currentState.equals(State.WAITING)){
			switch(action){
			case BUYFOOD:
				currentState = State.SHOPPING;
				break;
			case STEALMONEY:
				currentState = State.STEALINGMONEY;
				break;
			case STEALFOOD:
				currentState = State.STEALINGFOOD;
				break;
			case EAT:
				currentState = State.EATING;
				break;
			case SLEEP:
				currentState = State.SLEEPING;
				break;
			case WORK:
				currentState = State.WORKING;
				break;
			case UNKNOWN:
				break;			
			default:
				break;
			}
		}
		else if(action.equals(AgentAction.RETURN)) {
			currentState = State.HOME;
		}
		else if(action.equals(AgentAction.WAIT)) {
			currentState = State.WAITING;
		}
	}

	public void draw(Graphics2D create, ViewPanel viewPanel) {
		// draw expression in the environment
		switch(disEmotion){
		case ANGRY:
			create.setColor(Color.RED);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case DISGUST:
			create.setColor(Color.GREEN);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case FEAR:
			create.setColor(Color.BLACK);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case HAPPY:
			create.setColor(Color.YELLOW);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case NOTHING:
			create.setColor(Color.WHITE);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case SAD:
			create.setColor(Color.BLUE);
			create.fillRect(0, 0, viewPanel.getWidth(), viewPanel.getHeight());
			break;
		case UNKNOWN:
			break;
		default:
			break;
		}
	}

	//set displaying emotion text
	public void displayEmotion(String entity, AgentAction actorAction, String emotion) {
		
		env.getEnvWin().expression.setForeground(Color.BLACK);
		switch(emotion){
		case "happy":
			disEmotion = displayingEmotion.HAPPY;
			break;
		case "anger":
			disEmotion = displayingEmotion.ANGRY;
			break;
		case "sadness":
			disEmotion = displayingEmotion.SAD;
			break;
		case "fear":
			env.getEnvWin().expression.setForeground(Color.WHITE);
			disEmotion = displayingEmotion.FEAR;
			break;
		case "disgust":
			disEmotion = displayingEmotion.DISGUST;
			break;
		default:
			disEmotion = displayingEmotion.NOTHING;
			break;
		}
		
		env.getEnvWin().repaint();
	}
	
	//Translate the received emotion into natural language with multiple agents
	public void sayEmotion(String entity, AgentAction actorAction,
			String predicate, String agent1, String agent2, String object,
			int intensity) {
		JTextArea ta = env.getEnvWin().emotionText;
		String newText = ta.getText() + agent1+" is feeling " + predicate + " towards " + agent2 + " about " + object + " with intensity " + intensity + "\n";
		ta.setText(newText);
	}

	//Translate the received emotion in to natural language with one agent
	public void sayEmotion(String entity, AgentAction actorAction,
			String predicate, String agent, String object, int intensity) {
		JTextArea ta = env.getEnvWin().emotionText;
		String newText;
		if(predicate.equals("like") ||  predicate.equals("dislike") )
			newText = ta.getText() + agent +" " + predicate +"s " + object + " with intensity " + intensity+ "\n";
		else
			newText = ta.getText() + agent +" is feeling " + predicate + " about " + object + " with intensity " + intensity+ "\n";
		ta.setText(newText);
		
	}

}
